# 分布式系统架构设计

## 分布式系统架构设计概论
* [分布式系统架构设计](%E5%88%86%E5%B8%83%E5%BC%8F%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84%E8%AE%BE%E8%AE%A1.pdf)

## 开发
### 性能测试
* [性能测试](%E6%80%A7%E8%83%BD%E6%B5%8B%E8%AF%95.pdf)
### 数据存储
* [数据库SQL优化](%E6%95%B0%E6%8D%AE%E5%BA%93SQL%E4%BC%98%E5%8C%96.pdf)
* [SOAR](SOAR.pdf)
* [分布式数据库](%E5%88%86%E5%B8%83%E5%BC%8F%E6%95%B0%E6%8D%AE%E5%BA%93.pdf)
### NoSQL
* [NoSQL](NoSQL.pdf)
* [消息队列](%E6%B6%88%E6%81%AF%E9%98%9F%E5%88%97.pdf)
### 分布式事务
* [分布式事务](%E5%88%86%E5%B8%83%E5%BC%8F%E4%BA%8B%E5%8A%A1.pdf)

## 云原生
### 微服务
* [小议微服务的各种玩法](%E5%B0%8F%E8%AE%AE%E5%BE%AE%E6%9C%8D%E5%8A%A1%E7%9A%84%E5%90%84%E7%A7%8D%E7%8E%A9%E6%B3%95.pdf)
* [Spring Cloud](Spring%20Cloud.pdf)
* [Consul](Consul.pdf)
* [Envoy](Envoy.pdf)
### DevOps
* [Docker](Docker.pdf)
* [Dockerfile](Dockerfile.pdf)
* [Docker Compose](Docker%20Compose.pdf)
